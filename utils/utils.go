package utils

import (
	"fmt"
	"gopkg.in/telegram-bot-api.v4"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type connectionCredentials struct {
	Deluge struct {
		Url      string `yaml:"url"`
		Password string `yaml:"password"`
	}
	Telegram struct {
		Enabled   bool   `yaml:"enabled"`
		Token     string `yaml:"token"`
		Recipient int64  `yaml:"recipient"`
	}
	Rsync struct {
		User   string `yaml:"user"`
		Host   string `yaml:"host"`
		Source string `yaml:"source"`
		Mount  string `yaml:"mount"`
		Target string `yaml:"target"`
	}
}

var Credential connectionCredentials

func GetCreds(credLocation string) int {
	credsource, err := ioutil.ReadFile(credLocation)
	if err != nil {
		fmt.Printf("Can not open file %s\n", credLocation)
		return -1
	}
	err = yaml.Unmarshal(credsource, &Credential)
	if err != nil {
		fmt.Println("YAML file is invalid")
		return -2
	}
	return 0
}

func SendTelegramMessage(token string, recipient int64, message string) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		fmt.Println(err)
	}
	msg := tgbotapi.NewMessage(recipient, message)
	bot.Send(msg)
}
