package main

import (
	"bytes"
	"flag"
	"fmt"
	"gitlab.com/balasankarc/t3/utils"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

func listFiles(location string) ([]string, []string) {
	var files []string
	var dirs []string

	_, err := os.Stat(location)
	if os.IsNotExist(err) {
		return files, dirs
	}

	err = filepath.Walk(location, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			dirs = append(dirs, path)
		} else {
			files = append(files, info.Name())
		}
		return nil
	})
	if err != nil {
		fmt.Println("Error walking the directory")
	}
	return files, dirs
}

func main() {
	credLocation := flag.String("credential", "credential.yml", "Location of credentials file")
	flag.Parse()
	status := utils.GetCreds(*credLocation)
	if status != 0 {
		return
	}

	completedFiles, completedDirs := listFiles(utils.Credential.Rsync.Source)
	if len(completedFiles) == 0 {
		fmt.Println("No files")
		return
	}
	messageText := "Moved: \n"
	for _, file := range completedFiles {
		messageText += fmt.Sprintf("%s\n", file)
	}
	hostName := utils.Credential.Rsync.Host
	portNum := "22"
	seconds := 5
	timeOut := time.Duration(seconds) * time.Second

	_, err := net.DialTimeout("tcp", hostName+":"+portNum, timeOut)

	if err != nil {
		fmt.Println("Host not available")
		if utils.Credential.Telegram.Enabled {
			utils.SendTelegramMessage(utils.Credential.Telegram.Token, utils.Credential.Telegram.Recipient, "Host not available")
		}
		return
	} else {
		cmd := exec.Command("ssh", utils.Credential.Rsync.Host, fmt.Sprintf("mount | grep '%s'", utils.Credential.Rsync.Mount))
		var out bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &stderr
		err := cmd.Run()
		if err != nil {
			fmt.Println("Elements not mounted")
			if utils.Credential.Telegram.Enabled {
				utils.SendTelegramMessage(utils.Credential.Telegram.Token, utils.Credential.Telegram.Recipient, "Elements not mounted")
			}
			fmt.Println(stderr.String())
			fmt.Println(cmd.Args)
		} else {
			target_dir := fmt.Sprintf("%s@%s:%s/", utils.Credential.Rsync.User, utils.Credential.Rsync.Host, utils.Credential.Rsync.Mount)
			rsync_cmd := exec.Command("rsync", "--remove-source-files", "-avrqhe", "ssh", utils.Credential.Rsync.Source, target_dir)
			err := rsync_cmd.Run()
			var out bytes.Buffer
			var stderr bytes.Buffer
			rsync_cmd.Stdout = &out
			rsync_cmd.Stderr = &stderr
			if err != nil {
				fmt.Println(err)
				fmt.Println(stderr.String())
			} else {
				if utils.Credential.Telegram.Enabled {
					utils.SendTelegramMessage(utils.Credential.Telegram.Token, utils.Credential.Telegram.Recipient, messageText)
				}
				for _, file := range completedDirs {
					err := os.RemoveAll(file)
					if err != nil {
						fmt.Println(err)
					}
				}
			}
		}
	}
}
