# TV Torrent Taker (T3)

T3 is a cli tool written in Go to automatically download specific episodes of
your favorite TV shows from eztv.ag.

WARNING: Downloading TV Shows without their respective owners' permission is
deemed illegal in many countries. Use at your own risk.

## Requirements
1. A machine running amd64 kernel
1. Packages `deluge` and `deluge-web`
1. Both services `deluged` and `deluge-web` running.

## Installation
1. Download the executable and move it to a location in your `$PATH`. The
   following command saves it to `/usr/loca/bin/t3`

    ```
    $ sudo wget https://gitlab.com/balasankarc/t3/-/jobs/artifacts/master/raw/t3?job=compile -O /usr/local/bin/t3
    ```

## Usage
t3 depends on two configuration files. One listing details of the shows and
another listing credentials.

1. Configuration file storing details of shows (source.yml)
    ```
    $ cat source.yml
        
    shows:
    - name: Arrow
      url: https://eztv.ag/shows/679/arrow/
      season: "06"
      episode: "03"
    - name: Modern Family
      url: https://eztv.ag/shows/330/modern-family/
      season: "09"
      episode: "15"
    - name: The Flash
      url: https://eztv.ag/shows/1058/the-flash-2014/
      season: "03"
      episode: "07"
    ```
1. Configuration file storing credentials (credential.yml)
    ```
    $ cat credential.yml
       
    deluge:
      url: 127.0.0.1:8112
      password: deluged
    telegram:
      enabled: true
      token: TelegramBotAPIToken
      recipient: 123456
    ```
    
1. By default, t3 expects both the files are available in the same directory as
   t3 executable. But, you can specify their location using the flags `-source`
   and `-credential`. The following command initiates runs t3 and download the
   shows specified in source.yml.

    ```
    $ t3 -source <path to source.yml file> -credential <path to credential.yml file>
    ```

1. To list the available shows
    ```
    $ t3 -source <path to source.yml file> -list
     __________________________________
    |  ID  | Name                      |
    |______|___________________________|
    |  01  | Arrow                     |
    |  02  | Modern Family             |
    |  03  | The Flash                 |
    |  04  | Mr. Robot                 |
    |  05  | Agents of S.H.I.E.L.D     |
    |  06  | Silicon Valley            |
    |  07  | The Big Bang Theory       |
    |  08  | Episodes                  |
    |  09  | Westworld                 |
    |  10  | Black Mirror              |
    |  11  | Game of Thrones           |
    |  12  | Lucifer                   |
    |  13  | Man with a Plan           |
    |  14  | Young Sheldon             |
    |______|___________________________|
    
    ```

## Copyright and License

t3 is released under GPL-3.0+ license.  
Copyright: 2015-2018 Balasankar C <balasankarc@autistici.org>
