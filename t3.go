package main

import (
	"flag"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	deluge "github.com/brunoga/go-deluge"
	"gitlab.com/balasankarc/t3/utils"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

type DataSource struct {
	Shows []Show
}

type Show struct {
	Name      string `yaml:"name"`
	Directory string `yaml:"directory"`
	URL       string `yaml:"url"`
	Season    string `yaml:"season"`
	Episode   string `yaml:"episode"`
}

var Source DataSource

func listShows() {
	// Reads config.yml file and prints the available show information neatly
	var Shows []Show = Source.Shows
	if len(Shows) > 0 {
		fmt.Printf(" %s\n", strings.Repeat("_", 34))
		fmt.Printf("|  %s  | %-25s |\n", "ID", "Name")
		fmt.Printf("|______|_%s_|", strings.Repeat("_", 25))
		for i := 0; i < len(Shows); i++ {
			var show string = Shows[i].Name
			fmt.Printf("\n|  %02d  | %-25s |", i+1, show)
		}
		fmt.Printf("\n|______|_%s_|\n\n", strings.Repeat("_", 25))
	}
}

func numericSize(size_string string) float64 {
	// Convert size string to numerical value in MB
	size_string = strings.ToUpper(size_string)
	var in_mb float64 = 1000000.0000
	if strings.Contains(size_string, "GB") {
		numerical_string := size_string[0 : strings.Index(size_string, "GB")-1]
		gb_value, _ := strconv.ParseFloat(numerical_string, 64)
		in_mb = 1024.00 * gb_value
	} else if strings.Contains(size_string, "MB") {
		numerical_string := size_string[0 : strings.Index(size_string, "MB")-1]
		in_mb, _ = strconv.ParseFloat(numerical_string, 64)
	}
	return in_mb
}

func findSmallest(links map[string]float64) (string, float64) {
	// Find smallest torrent from list
	smallest := 100000000.00
	smallest_link := ""
	for link, size := range links {
		if size < smallest {
			smallest = size
			smallest_link = link
		}
	}
	return smallest_link, smallest
}

func findMatching(url string, season_string_needed string) map[string]float64 {
	// Get list of URLs matching with the required episode
	regexes := [2]*regexp.Regexp{regexp.MustCompile("S(?P<season>[0-9]+)E(?P<episode>[0-9]+)"), regexp.MustCompile("(?P<season>[0-9]*)x(?P<episode>[0-9]*)")}
	var re *regexp.Regexp
	sizeInfo := map[string]float64{}

	doc, err := goquery.NewDocument(url)
	if err != nil {
		fmt.Println(err)
	}

	// Finding rows that correspond to each episode
	doc.Find(".forum_header_border").Each(func(i int, row *goquery.Selection) {
		// Finding each column in the row
		info := row.Find(".forum_thread_post").First()
		name_node := info.Next()
		links_node := name_node.Next()
		size_node := links_node.Next()
		name := name_node.Text()

		regexp_match := false
		for _, re = range regexes {
			if re.MatchString(name) {
				regexp_match = true
				break
			}
		}
		if regexp_match {
			n1 := re.SubexpNames()
			r2 := re.FindStringSubmatch(name)
			matches := map[string]int{}
			for i, n := range r2 {
				key := n1[i]
				value, _ := strconv.Atoi(n)
				matches[key] = value
			}
			season_string := fmt.Sprintf("S%02dE%02d", matches["season"], matches["episode"])
			if season_string == season_string_needed {
				magnet_link, _ := links_node.Find(".magnet").Attr("href")
				size_string := size_node.Text()
				sizeInfo[magnet_link] = numericSize(size_string)
			}
		}
	})
	return sizeInfo
}

func process(wg *sync.WaitGroup, channel chan<- bool, show Show, delugeClient *deluge.Deluge, Source string) {
	// Process each show
	defer wg.Done()
	season_string := fmt.Sprintf("S%sE%s", show.Season, show.Episode)
	fmt.Println(show.Name, " : ", season_string)
	sizeInfo := findMatching(show.URL, season_string)
	status := false
	if len(sizeInfo) > 0 {
		link, _ := findSmallest(sizeInfo)
		move_dir := fmt.Sprintf("%s/%s/Season_%s", Source, show.Directory, show.Season)
		_ = os.MkdirAll(move_dir, 0777)
		// In case umask prevented the previous setting
		_ = os.Chmod(move_dir, 0777)
		options := map[string]interface{}{"move_completed": "true", "move_completed_path": move_dir}
		delugeClient.CoreAddTorrentMagnet(link, options)
		status = true
	}
	channel <- status
}

func findNextEpisode(season string, episode string) (string, string) {
	current_episode, _ := strconv.Atoi(episode)
	current_season, _ := strconv.Atoi(season)
	new_episode := fmt.Sprintf("%02d", ((current_episode + 1) % 25))
	new_season := fmt.Sprintf("%02d", current_season)
	if new_episode == "00" {
		new_episode = "01"
		new_season = fmt.Sprintf("%02d", (current_season + 1))
	}
	return new_season, new_episode
}

func main() {
	var messageText string = "Added: \n"
	var wg sync.WaitGroup
	listStatus := flag.Bool("list", false, "List shows")
	sourceLocation := flag.String("source", "source.yml", "Location of source file")
	credLocation := flag.String("credential", "credential.yml", "Location of credentials file")
	flag.Parse()

	// Parse source file to Source variable
	source, err := ioutil.ReadFile(*sourceLocation)
	if err != nil {
		fmt.Printf("Can not open file %s\n", *sourceLocation)
		return
	}
	err = yaml.Unmarshal(source, &Source)
	if err != nil {
		fmt.Println("YAML file is invalid")
		return
	}

	// Check if user gave the `-list` option
	if *listStatus {
		listShows()
		return
	}

	// Parse credential file to Credential variable
	status := utils.GetCreds(*credLocation)
	if status != 0 {
		return
	}

	// Create deluge client
	delugeClient, err := deluge.New("http://"+utils.Credential.Deluge.Url+"/json", utils.Credential.Deluge.Password)
	if err != nil {
		fmt.Println("Error creating deluge client")
		return
	}

	// Process each show. Makes use of goroutines to parallely process each show
	// and channels to pass information regarding whether an episode was found or
	// not back to main.
	for index, item := range Source.Shows {
		wg.Add(1)
		channel := make(chan bool)
		go process(&wg, channel, item, delugeClient, utils.Credential.Rsync.Source)

		status := <-channel
		if status {
			fmt.Printf("Found new episode of %s and added.\n", item.Name)
			messageText += fmt.Sprintf("%s S%sE%s\n", item.Name, item.Season, item.Episode)

			new_season, new_episode := findNextEpisode(item.Season, item.Episode)
			Source.Shows[index].Season = new_season
			Source.Shows[index].Episode = new_episode
		} else {
			fmt.Printf("No new episodes of %s found.\n", item.Name)
		}
	}

	if messageText != "Added: \n" {
		// If telegram notification is enabled, send notification
		if utils.Credential.Telegram.Enabled {
			utils.SendTelegramMessage(utils.Credential.Telegram.Token, utils.Credential.Telegram.Recipient, messageText)
		}
	}
	// Write back the new source file
	yaml_string, _ := yaml.Marshal(&Source)
	ioutil.WriteFile(*sourceLocation, yaml_string, 0644)
	wg.Wait()
}
